# CarSpotting


## Login to AWS

### Linux

```bash
ssh -i key.pem ubuntu@18.194.207.103  # public one
ssh -i key.pem ubuntu@18.197.129.74  # my personal machine
```

### Windows

* download putty.exe
* enter ip adress
* go to Connection->SSH->Auth
* click on browse and choose the private key file
* go back to Session and give the session a name and save (you can double click on the name later and load it automatically)

### AWS IP adresses
jayjit: 18.197.136.244
samuel: 
soumya: 54.93.235.3
theodor: 18.185.123.104

### Clone the repo
Generate and add your ssh key to bitbucket ([see here](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html))
```bash
git clone git@bitbucket.org:carspotting/carspotting.git
```

