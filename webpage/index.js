const express = require('express');
const bodyParser = require('body-parser');
const request = require('request')
const formidable = require('formidable')
const fs = require('fs')

const app = express();

app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));
app.set('view engine', 'ejs')



// At the top of your server.js
process.env.PWD = process.cwd()

// Then
app.use(express.static(process.env.PWD + '/uploads'));

app.get('/', function (req, res) {
   res.render('index', {uploadedImage: null, prediction: null, error: null});
})

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
})

app.post('/', function (req, res) {
  res.render('index');
})

var itmp = null;
var pathForScript = null;

app.post('/upload', function (req, res) {

var form = new formidable.IncomingForm();
form.parse(req, function(err, fields, files) {
	var path = files.filetoupload.path;// + '/' + files.filetoupload.name;
	console.log(files);
	
	fs.readFile(path, function(err, data) {
		if(!path){
			console.log("No path given");
			res.redirect("/")
		}
		else {
			console.log("Path given")
			console.log(data);
			var newPath = __dirname + "/uploads/" + files.filetoupload.name;
			pathForScript = newPath;
			console.log(newPath);
			fs.writeFile(newPath, data, function(err) {
				console.log(err);
			});
					itmp = "/" + files.filetoupload.name;
					
					
					
					    var spawn = require("child_process").spawn;
						var arg = "--image " + pathForScript;
						console.log(arg); 
						var process = spawn('python',["../model_execution/classifier_dummy.py"], [arg]);
					//    var process = spawn('python',["../model_execution/classify.py"], [arg]);
						process.stdout.on('data', function(data) {
						console.log("Here");
						res.render('index', {uploadedImage: itmp, prediction: data.toString(), error: null});
							//res.send(data.toString());
						} )
					
					
					//res.render('index', {uploadedImage: itmp, prediction: , error: null});
			
		}
	});
	
	

})
});

app.post('/predict', function (req, res) {

    var spawn = require("child_process").spawn;
	var arg = "--image " + pathForScript;
	console.log(arg); 
    var process = spawn('python',["../model_execution/classifier_dummy.py"], [arg]);
//    var process = spawn('python',["../model_execution/classify.py"], [arg]);
    process.stdout.on('data', function(data) {
	console.log("Here");
	res.render('index', {prediction: data.toString(), error: null});
        //res.send(data.toString());
    } )

});
